%global ibus_tables_dir %{_datadir}/ibus-table/tables
%global ibus_icons_dir %{_datadir}/ibus-table/icons
Name:           ibus-table-chinese
Version:        1.8.12
Release:        4
Summary:        IBus Chinese input tables
License:        GPL-3.0-or-later
URL:            https://github.com/definite/ibus-table-chinese
Source0:        https://github.com/mike-fabian/ibus-table-chinese/releases/download/%{version}/ibus-table-chinese-%{version}.tar.gz
Patch0:         ibus-table-chinese-1.8.12-port-to-newer-cmake.patch
BuildRequires:  cmake >= 2.6.2 ibus-table-devel >= 1.2.0
Requires:       ibus-table >= 1.2.0
Obsoletes:      ibus-table-yinma < 1.3 ibus-table-xingma < 1.3

BuildArch:      noarch

%description
ibus-table-chinese provides the Chinese input methods infrastructure.

%package        array
Summary:        Array input methods
License:        CC0-1.0 and GPL-3.0-or-later
Requires:       %{name} = %{version}-%{release}
Provides:       ibus-table-array30 = %{version}-%{release}
Obsoletes:      ibus-table-array30 < 1.3

%description    array
Array is a free, open-minded character-structured input method.

%package        cangjie
Summary:        Cangjie based input methods
License:        CC0-1.0 and GPL-3.0-or-later
Requires:       %{name} = %{version}-%{release}
Provides:       ibus-table-cangjie = %{version}-%{release}
Obsoletes:      ibus-table-cangjie < 1.3

%description    cangjie
Cangjie based input methodses.

%package        cantonese
Summary:        Cantonese input methods
License:        GPL-2.0-only AND GPL-3.0-or-later AND MIT
Requires:       %{name} = %{version}-%{release}
Provides:       ibus-table-cantonese = %{version}-%{release}
Obsoletes:      ibus-table-cantonese < 1.3

%description    cantonese
Cantonese includes Cantonese, Hong-Kong version of Cantonese and jyutping.

%package        easy
Summary:        Easy input method
License:        GPL-2.0-only
Requires:       %{name} = %{version}-%{release}
Provides:       ibus-table-easy = %{version}-%{release}
Obsoletes:      ibus-table-easy < 1.3

%description    easy
Easy phrase-wise input method.

%package        erbi
Summary:        Erbi input method
License:        GPL-2.0-or-later
Requires:       %{name} = %{version}-%{release}
Provides:       ibus-table-erbi = %{version}-%{release}
Obsoletes:      ibus-table-erbi < 1.3

%description    erbi
Erbi  Includes Super Erbi and Erbi Qin-Song.

%package        quick
Summary:        Quick-to-learn input methods
License:        CC0-1.0 and GPL-3.0-or-later
Requires:       %{name} = %{version}-%{release}
Provides:       ibus-table-quick = %{version}-%{release}
Obsoletes:      ibus-table-quick < 1.3

%description    quick
Quick is based on Cangjie input method.

%package        scj
Summary:        Smart Cangjie
License:        GPL-3.0-or-later
Requires:       %{name} = %{version}-%{release}
Provides:       ibus-table-cangjie = %{version}-%{release}
Obsoletes:      ibus-table-cangjie < 1.3

%description    scj
Smart Cangjie is an improved Cangjie base input method.

%package        stroke5
Summary:        Stroke 5 input method
License:        GPL-3.0-or-later
Requires:       %{name} = %{version}-%{release}
Provides:       ibus-table-stroke5 = %{version}-%{release}
Obsoletes:      ibus-table-stroke5 < 1.3 ibus-table-yinma < 1.3

%description    stroke5
Stroke5 input method.

%package        wu
Summary:        Wu pronunciation input method
License:        GPL-2.0-or-later
Requires:       %{name} = %{version}-%{release}
Provides:       ibus-table-wu = %{version}-%{release}
Obsoletes:      ibus-table-wu < 1.3

%description    wu
Wu pronunciation input method.

%package        wubi-haifeng
Summary:        Haifeng Wubi input method
License:        0BSD
Requires:       %{name} = %{version}-%{release}
Provides:       ibus-table-wubi = %{version}-%{release}
Obsoletes:      ibus-table-wubi < 1.3

%description    wubi-haifeng
Haifeng Wubi current includes Haifeng Wubi 86.

%package        wubi-jidian
Summary:        Jidian Wubi 86 input method
License:        CC0-1.0 and GPL-3.0-or-later
Requires:       %{name} = %{version}-%{release}
Provides:       ibus-table-wubi = %{version}-%{release}
Obsoletes:      ibus-table-wubi < 1.3

%description    wubi-jidian
Jidian Wubi current includes Wubi 86.

%package        yong
Summary:        YongMa input method
License:        GPL-3.0-only
Requires:       %{name} = %{version}-%{release}
Provides:       ibus-table-yong = %{version}-%{release}
Obsoletes:      ibus-table-yong < 1.3

%description    yong
YongMa input method.

%package cantonyale
Summary:        Cantonese input method based on yale romanization
License:        GPL-2.0-only
Requires:       %{name} = %{version}-%{release}
 
%description cantonyale
Cantonese input method based on yale romanization

%prep
%autosetup -n %{name}-%{version} -p1

%build
%cmake -DMANAGE_MESSAGE_LEVEL=6 .
%cmake_build

%install
%cmake_install

# Register as AppStream components to be visible in the software center
mkdir -p $RPM_BUILD_ROOT%{_datadir}/metainfo
cp metainfo/*.appdata.xml $RPM_BUILD_ROOT%{_datadir}/metainfo

rm -fr %{buildroot}%{_docdir}/*

%files
%license COPYING
%doc AUTHORS README ChangeLog
 
%files array
%{_datadir}/metainfo/ibus-table-chinese-array.appdata.xml
%{ibus_icons_dir}/array30.*
%{ibus_tables_dir}/array30.db
%{ibus_icons_dir}/array30-big.*
%{ibus_tables_dir}/array30-big.db
 
%files cangjie
%{_datadir}/metainfo/ibus-table-chinese-cangjie.appdata.xml
%{ibus_icons_dir}/cangjie3.*
%{ibus_tables_dir}/cangjie3.db
%{ibus_icons_dir}/cangjie5.*
%{ibus_tables_dir}/cangjie5.db
%{ibus_icons_dir}/cangjie-big.*
%{ibus_tables_dir}/cangjie-big.db
 
%files cantonese
%{_datadir}/metainfo/ibus-table-chinese-cantonese.appdata.xml
%{ibus_icons_dir}/cantonese.*
%{ibus_tables_dir}/cantonese.db
%{ibus_icons_dir}/cantonhk.*
%{ibus_tables_dir}/cantonhk.db
%{ibus_icons_dir}/jyutping.*
%{ibus_tables_dir}/jyutping.db
 
%files easy
%{_datadir}/metainfo/ibus-table-chinese-easy.appdata.xml
%{ibus_icons_dir}/easy-big.*
%{ibus_tables_dir}/easy-big.db
 
%files erbi
%{_datadir}/metainfo/ibus-table-chinese-erbi.appdata.xml
%{ibus_icons_dir}/erbi.*
%{ibus_tables_dir}/erbi.db
%{ibus_icons_dir}/erbi-qs.*
%{ibus_tables_dir}/erbi-qs.db
 
%files quick
%{_datadir}/metainfo/ibus-table-chinese-quick.appdata.xml
%{ibus_icons_dir}/quick3.*
%{ibus_tables_dir}/quick3.db
%{ibus_icons_dir}/quick5.*
%{ibus_tables_dir}/quick5.db
%{ibus_icons_dir}/quick-classic.*
%{ibus_tables_dir}/quick-classic.db
 
%files scj
%{_datadir}/metainfo/ibus-table-chinese-scj.appdata.xml
%{ibus_icons_dir}/scj6.*
%{ibus_tables_dir}/scj6.db
 
%files stroke5
%{_datadir}/metainfo/ibus-table-chinese-stroke5.appdata.xml
%{ibus_icons_dir}/stroke5.*
%{ibus_tables_dir}/stroke5.db
 
%files wu
%{_datadir}/metainfo/ibus-table-chinese-wu.appdata.xml
%{ibus_icons_dir}/wu.*
%{ibus_tables_dir}/wu.db
 
%files wubi-haifeng
%{_datadir}/metainfo/ibus-table-chinese-wubi-haifeng86.appdata.xml
%doc tables/wubi-haifeng/COPYING tables/wubi-haifeng/README
%{ibus_icons_dir}/wubi-haifeng86.*
%{ibus_tables_dir}/wubi-haifeng86.db
 
%files wubi-jidian
%{_datadir}/metainfo/ibus-table-chinese-wubi-jidian86.appdata.xml
%{ibus_icons_dir}/wubi-jidian86.*
%{ibus_tables_dir}/wubi-jidian86.db
 
%files yong
%{_datadir}/metainfo/ibus-table-chinese-yong.appdata.xml
%{ibus_icons_dir}/yong.*
%{ibus_tables_dir}/yong.db
 
%files cantonyale
%{_datadir}/metainfo/ibus-table-chinese-cantonyale.appdata.xml
%{ibus_icons_dir}/cantonyale.*
%{ibus_tables_dir}/cantonyale.db

%changelog
* Tue Mar 04 2025 Funda Wang <fundawang@yeah.net> - 1.8.12-4
- try build with cmake 4.0

* Wed Nov 06 2024 Funda Wang <fundawang@yeah.net> - 1.8.12-3
- adopt to new cmake macro

* Thu Jul 18 2024 yaoxin <yao_xin001@hoperun.com> - 1.8.12-2
- License compliance rectification

* Wed Sep 06 2023 wangkai <13474090681@163.com> - 1.8.12-1
- Update to 1.8.12
- Change source url to github.com/mike-fabian/ibus-table-chinese 

* Mon Apr 20 2020 lizhenhua <lizhenhua21@huawei.com> - 1.8.2-13
- Package init
